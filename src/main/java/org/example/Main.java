package org.example;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;

public class Main {

    private static final String PASS_PHRASE = "1234";

    public static void main(String[] args) {
        String input = loadJson();
        // get rid of the pretty printing to produce compact JSON
        String json = (new Gson()).fromJson(input, JsonObject.class).toString();
        System.out.println(json);
        System.out.println(); // empty line to make the output clearer
        System.out.println(getSignature(json));
    }

    private static String loadJson() {
        try (var inStream = Main.class.getClassLoader().getResourceAsStream("input.json")) {
            assert inStream != null;
            return new String(inStream.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getSignature(String jsonBody) {
        // usually this would be a path to some file location
        // it's written this way to reference a class path resource just to make the code portable
        try (InputStream ksFile = Main.class.getClassLoader().getResourceAsStream("keystore.p12")) {
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(ksFile, PASS_PHRASE.toCharArray());
            // there may be more than one key in the keystore...here we are picking the 'example' key
            var pk = ks.getKey("example", PASS_PHRASE.toCharArray());

            Signature ps = Signature.getInstance("SHA256withRSA");

            ps.initSign((PrivateKey) pk);
            ps.update(jsonBody.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(ps.sign());
        } catch (KeyStoreException | SignatureException | NoSuchAlgorithmException | CertificateException |
                 InvalidKeyException | UnrecoverableKeyException | IOException e) {
            throw new RuntimeException("signature failure");
        }
    }
}